<?php

namespace frontend\models;

use Yii;
use common\models\User;
use frontend\models\Comment;

/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $game_cover
 * @property integer $creator
 * @property string $updated_at
 * @property string $created_at
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['creator'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['title', 'game_cover'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'game_cover' => 'Game Cover',
            'creator' => 'Creator',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
	
	public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator']);
    }
	
	public function getComments()
    {
        return $this->hasMany(Comment::className(), ['game_id' => 'id']);
    }
}
