<?php
namespace frontend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;

class GameCoverUpload extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
	private $upload_path = "uploads/game-cover/";
	
    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
		if(!$this->imageFile)
			return false;
        if ($this->validate()) {
			if(!file_exists($this->upload_path)) {
				mkdir($this->upload_path, 0775, true);
			}
			$filename = time().uniqid().'.'.$this->imageFile->extension;
			$path = $this->upload_path . $filename;
			Image::frame($this->imageFile->tempName)
				->thumbnail(new Box(400, 400))
				->save($path, ['quality' => 100]);
			return $path;
        } else {
            return false;
        }
    }
}