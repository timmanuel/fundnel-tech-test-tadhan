<?php
return [
    'createPost' => [
        'type' => 2,
        'description' => 'Create a post',
    ],
    'updatePost' => [
        'type' => 2,
        'description' => 'Update post',
    ],
    'deletePost' => [
        'type' => 2,
        'description' => 'Delete post',
    ],
    'createComment' => [
        'type' => 2,
        'description' => 'Post Comment',
    ],
    'updateOwnPost' => [
        'type' => 2,
        'description' => 'Update own post',
        'ruleName' => 'isMember',
        'children' => [
            'updatePost',
        ],
    ],
    'member' => [
        'type' => 1,
        'children' => [
            'createPost',
            'updateOwnPost',
            'updatePost',
            'createComment',
        ],
    ],
];
