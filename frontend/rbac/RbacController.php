<?php
namespace frontend\rbac;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
		$auth = \Yii::$app->authManager;
		
		// add "createPost" permission
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
        $auth->add($createPost);

        // add "updatePost" permission
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Update post';
        $auth->add($updatePost);
		
		// add "deletePost" permission
        $deletePost = $auth->createPermission('deletePost');
        $deletePost->description = 'Delete post';
        $auth->add($deletePost);
		
		// add "createComment" permission
		$createComment = $auth->createPermission('createComment');
        $createComment->description = 'Post Comment';
        $auth->add($createComment);
		
		$rule = new \app\rbac\MemberRule;
		$auth->add($rule);
		// add the "updateOwnPost" permission and associate the rule with it.
		$updateOwnPost = $auth->createPermission('updateOwnPost');
		$updateOwnPost->description = 'Update own post';
		$updateOwnPost->ruleName = $rule->name;
		$auth->add($updateOwnPost);
		// "updateOwnPost" will be used from "updatePost"
		$auth->addChild($updateOwnPost, $updatePost);
		
        // add "member" role and give this role the permissions 
        $member = $auth->createRole('member');
        $auth->add($member);
        $auth->addChild($member, $createPost);
        $auth->addChild($member, $updateOwnPost);
        $auth->addChild($member, $updatePost);
        $auth->addChild($member, $createComment);
    }
}