<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Game;
use frontend\models\GameSearch;
use frontend\models\GameCoverUpload;
use frontend\models\Comment;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\web\Request;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GameController implements the CRUD actions for Game model.
 */
class GameController extends Controller
{
	public function behaviors()
    {
		date_default_timezone_set('Asia/Jakarta');
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Game models.
     * @return mixed
     */
	 
    public function actionIndex()
    {
		$searchModel = new GameSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Game model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		if (!(\Yii::$app->user->can('createPost'))) {
			return $this->redirect(array('game/'));
		}
		
		return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    /**
     * Creates a new Game model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		if (!(Yii::$app->user->can('createPost'))) {
			return $this->redirect(array('game/'));
		}
		
        if ($model->load(Yii::$app->request->post())){
			$model = new Game();
			$model->creator = Yii::$app->user->getId();
			$model->created_at = date('Y-m-d H:i:s');
			$model->updated_at = date('Y-m-d H:i:s');
			
			if($model->save()){
				return $this->redirect(['view', 'id' => $model->id]);
			}
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Game model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$current_game_cover = $model->game_cover;
		if (!(Yii::$app->user->can('updateOwnPost',['post'=>$model]))) {
			return $this->redirect(array('game/'));
		}
		
		if ($model->load(Yii::$app->request->post())){
			$game_cover = new GameCoverUpload();
			$game_cover->imageFile = UploadedFile::getInstance($model, 'game_cover');
			$upload = $game_cover->upload();
			
			if ($upload) { //there's a new game cover
				if(!empty($current_game_cover)){
					if(file_exists($current_game_cover)){
						unlink($current_game_cover);
					}
				}
				$model->game_cover = $upload;
			}
			else{
				$model->game_cover = $current_game_cover;
			}
			
			$model->updated_at = date('Y-m-d H:i:s');
			
			if($model->save()){
				return $this->redirect(['view', 'id' => $model->id]);
			}
		}     
        else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Game model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$model = $this->findModel($id);
		if (Yii::$app->user->can('updateOwnPost',['post'=>$model])) {
			$model->delete();
		}
        return $this->redirect(['index']);
    }
	
	 /**
     * Displays a Game Page.
     * @param integer $id
     * @return mixed
     */
	public function actionPage($id)
    {
		$model = $this->findModel($id);
		$creator = $model->getCreator()->one();
		$comments = $model->getComments()->with('user')->all();
		return $this->render('page', [
                'model' => $model,
                'creator' => $creator,
                'comments' => $comments,
            ]);
    }

	public function actionComment()
	{
		if (Yii::$app->request->isAjax) {
			$return['success'] = 0;
			if (Yii::$app->user->can('createComment')) {
				$data = Yii::$app->request->post();
				$identity = Yii::$app->user->identity;
				$model = new Comment();
				$model->user_id = $identity['id'];
				$model->game_id = $data['game_id'];
				$model->comment = $data['comment'];
				$model->created_at = date('Y-m-d H:i:s');
				$model->updated_at = date('Y-m-d H:i:s');
				if($model->save()){
					$return['data']['created_at'] = $model->created_at;
					$return['data']['comment'] = Html::encode($model->comment);
					$return['data']['username'] = $identity['username'];
					$return['success'] = 1;
				}
				else{
					$return['message'] = "Failed writing to database. Please contact the administrator";
				}
			}
			else{
				$return['message'] = "You don't have permission to comment on this page. Please log in first";
			}
			echo json_encode($return);
		}
		else{
			throw new \yii\web\NotFoundHttpException();
		}
		return false;
	}
    /**
     * Finds the Game model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Game the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Game::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
