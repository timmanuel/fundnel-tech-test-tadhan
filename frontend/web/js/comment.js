$(document).ready(function(){
	$("#txtComment").val("");
	$("#btnComment").click(function(){
		var comment = $("#txtComment").val();
		if(comment == ""){
			alert("Comment cannot be empty");
			return false;
		}
		var game_id = $("#hidId").val();
		
		var url = base_url+"game/comment";
		$.ajax({
			url: url,
			type: 'POST',
			data: {comment:comment,game_id:game_id},
			dataType: 'json',
			success: function(data) {
				if(data.success == 1) {
					var toAppend = '<div class="row">';
						toAppend += '<h5 class="text-primary col-sm-6"><strong>'+data.data.username+'</strong></h5>';
						toAppend += '<h5 class="col-sm-6"><small>'+data.data.created_at+'</small></h5>';
					toAppend += '</div>';
					toAppend += '<div class="row">';
					  toAppend += '<p class="col-sm-12">'+data.data.comment+'</p>';
					toAppend += '</div>';
					toAppend += '<hr />';
					$("#js-list-comment").append(toAppend);
					$("#txtComment").val("");
				}
				else{
					alert(data.message);
				}
			}
		});
	});
});

