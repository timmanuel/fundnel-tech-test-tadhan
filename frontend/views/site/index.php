<?php

/* @var $this yii\web\View */
use common\helper\UrlHelper;
$this->title = 'Online game portal';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Online game portal<h1>

        <p class="lead">This is a simple portal for users to read, post and discuss about games. </p>

        <p><a class="btn btn-lg btn-success" href="<?= Yii::$app->urlManager->baseUrl;?>/game/index">View Game</a></p>
    </div>

  
</div>
