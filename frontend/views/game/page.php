<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helper\UrlHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\Game */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(UrlHelper::js_url().'comment.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="game-view">
	<div class="container-fluid">
		<div class="row">
		  <div class="col-sm-12 col-md-6"><h2><?= Html::encode($this->title) ?></h2></div>
		  <div class="col-sm-12 col-md-6" style="vertical-align: bottom; float:right"><h4><small>Page created by : <?= Html::encode($creator->username) ?></small></h4></div>
		</div>
		<div class="row">
		  <div class="col-sm-12"><p><?= Html::encode($model->description) ?></p></div>
		</div>
		<?php if($model->game_cover):?>
		<div class="row">
			<div class="col-sm-12"><p>
			<?= Html::img(Yii::$app->urlManager->baseUrl . '/' . $model->game_cover, [
					'alt'=>Yii::t('app', 'Game Cover of ') . $model->title
				]); ?>
			</p></div>
		</div>
		<?php endif;?>
		<div class="row">
		  <div class="col-sm-12"><h3>Comment</h3></div>
		</div>
		<div id="js-list-comment">
		<?php foreach($comments as $comment):?>
		<div class="row">
			<h5 class="text-primary col-sm-6"><strong><?= $comment->user->username;?></strong></h5>
			<h5 class="col-sm-6"><small><?= $comment->created_at;?></small></h5>
		</div>
		<div class="row">
		  <p class="col-sm-12"><?= Html::encode($comment->comment);?></p>
		</div>
		<hr />
		<?php endforeach;?>
		</div>
		<div class="row">
		  <textarea id="txtComment" class="col-sm-12 col-md-6"></textarea>
		  <input type="hidden" id="hidId" value="<?= $model->id; ?>" />
		</div>
		<div class="row">
			<p><button id="btnComment">Comment</button></p>
		</div>
	</div>
</div>
