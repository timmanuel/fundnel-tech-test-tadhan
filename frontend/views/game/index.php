<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Yii::$app->user->isGuest?'':Html::a('Create Game', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
           // 'description:ntext',
            [
				'label'=>'View Detail',
				'format' => 'raw',
				'value'=> function ($data) {
					return Html::a("View Detail",array('game/page/'.$data->id));
				},
			],
            [
				'label'=>'Action',
				'content' => function ($model, $key, $index, $column) {
					if (Yii::$app->user->can('updateOwnPost',['post'=>$model])) {
						return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->id])
						.' '. Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id])
						.' '. Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], ['data-method'=> 'post']);
					}
				},
				'visible' => !Yii::$app->user->isGuest
			]
        ],
    ]); ?>

</div>
