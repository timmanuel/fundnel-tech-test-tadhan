<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helper\UrlHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\Game */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
	<div class="form-group field-game-game_cover">
	<label class="control-label" for="game-game_cover">Game Cover</label>
	<?php if($model->game_cover):?>
	<p>
	<?= Html::img(Yii::$app->urlManager->baseUrl . '/' . $model->game_cover, [
            'alt'=>Yii::t('app', 'Game Cover of ') . $model->title
        ]); ?></p>
	<?php endif;?>
	<input type="hidden" name="Game[game_cover]" value=""><input type="file" id="game-game_cover" name="Game[game_cover]" value="uploads/game-cover/1451239359568027bf06e71.jpg">

	<div class="help-block"></div>
	</div>
	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
		<a href="<?= UrlHelper::frontend_base_url();?>/game"/><?= Html::Button('Back to List', ['class' => 'btn btn-primary']) ?></a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
