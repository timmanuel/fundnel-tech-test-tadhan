<?php
namespace common\helper;

use yii\helpers\Url;

class UrlHelper
{
    public static function frontend_base_url() {
        return Url::base();
    }
	
	public static function css_url() {
        return Url::base()."/css/";
    }
	
	public static function js_url() {
        return Url::base()."/js/";
    }
}