# fundnel
This is Fundnel Tech Test using Yii2 Framework

Preparation :
1. In your application root, run 'php init' via command line  
2. Create a mysql database  
3. In your application root, rename example.env into config.env then edit the file with your own configuration
4. Run the migration with 'yii migrate' command via command line
5. The test was done in the frontend section, so go to /frontend/web/ to access it