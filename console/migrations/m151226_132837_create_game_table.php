<?php

use yii\db\Schema;
use yii\db\Migration;

class m151226_132837_create_game_table extends Migration
{
    public function up()
    {
		$this->createTable('game', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
			'game_cover' => $this->string(),
			'creator' => $this->integer(),
			'updated_at' => $this->timestamp(),
			'created_at' => $this->timestamp()
        ]);
    }

    public function down()
    {
         $this->dropTable('game');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
