<?php

use yii\db\Schema;
use yii\db\Migration;

class m151229_161732_create_comment_table extends Migration
{
    public function up()
    {
		$this->createTable('comment', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'game_id' => $this->integer()->notNull(),
            'comment' => $this->text(),
			'updated_at' => $this->timestamp(),
			'created_at' => $this->timestamp()
        ]);
    }

    public function down()
    {
        $this->dropTable('comment');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
